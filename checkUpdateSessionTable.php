<?php
/**
 * Plugin to check and update DBsession table until find a better fix
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.0.3-dev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class checkUpdateSessionTable extends PluginBase {

    static protected $description = 'Check and update DB session table until find a better fix for msSQL database';
    static protected $name = 'checkUpdateSessionTable';

    /**
    * @var array[] the settings
    */
    protected $settings = array(
        'currentDbSettings' => array(
            'type' => 'info',
            'content' => 'Current data column is “%s” type.',
            'class'=>"well lead",
        ),
        'setToDbSettings' => array(
            'type'=>'select',
            'options'=>array(
                'text'=>"Text",
                'longbinary'=>"Binary",
            ),
            'htmlOptions'=>array(
                'empty'=>"No update",
            ),
            'label'=>'Set data column to type. <strong>',
            'default'=>"",
        ),
        'warningDbSettings' => array(
            'type' => 'info',
            'content' => 'If you use DB session : updating column type broke your current session and can disable new login.',
            'class'=>"alert alert-warning",
        ),
    );

    public function init()
    {

    }

    /**
     * @see parent:getPluginSettings
     */
    public function getPluginSettings($getValues=true)
    {
        $pluginSettings= parent::getPluginSettings($getValues);
        if(Yii::app()->getRequest()->getPost("setToDbSettings")) {
            $pluginSettings['currentDbSettings']['content'] = CHtml::link($this->gT("You just update the setting, click to reload."),array("admin/pluginmanager/sa/configure",'id'=>$this->id));
            return $pluginSettings;
        }
        $tableSession = Session::model()->tableName();
        $currentType = Yii::app()->getDb()->getSchema()->getTable($tableSession)->getColumn('data')->dbType;
        $pluginSettings['currentDbSettings']['content'] = sprintf($this->gT("Current data column is “%s” type."),"<strong>".$currentType."</strong>");
        return $pluginSettings;
    }

    public function saveSettings($settings)
    {
        if(!empty($settings['setToDbSettings'])) {
            $tableSession = Session::model()->tableName();
            switch($settings['setToDbSettings']) {
                case 'text':
                    Yii::app()->db->createCommand()->dropColumn($tableSession ,'data');
                    Yii::app()->db->createCommand()->addColumn($tableSession ,'data', 'text');
                    break;
                case 'longbinary':
                    Yii::app()->db->createCommand()->dropColumn($tableSession ,'data');
                    Yii::app()->db->createCommand()->addColumn($tableSession ,'data', 'longbinary');
                    break;
                default:
                    // Unknow type : do nothing
            }
        }
    }

}
